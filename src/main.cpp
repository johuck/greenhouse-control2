#include <Arduino.h>
#undef SYSTEMU_DEBUG
#include <systemu.h>
#include "greenhouse.h"
#include "pins.h"
#include <FS.h>


TemperatureConfig temperature_config = {
    CONTROL_MODE_DISABLED, /* control_mode */
    false,                  /* farenheit? */
    100.0,                  /* current_temperature */
    100.0,                  /* current_humidity */
    25.0,                   /* target */
    1.0,                    /* tolerance */
};

WateringConfig watering_config = {
    CONTROL_MODE_DISABLED, /* control_mode */
    20,                     /* min_percentage */
    40,                     /* max_percentage */
    0,                      /* current_percentage */
    false,                  /* manual_state */
    MoistureSensorCalibration {
        0,
        1023
    }
};

WifiCredentials wifi_credentials = {
    "Huck!Lan",      /* ssid */
    "HuckSpeed2018+" /* password */
};

Scheduler scheduler;

Service *services[] = {
    &temperature_updater_service, 
    &temperature_control_service,
    &watering_moisture_update_service,
    &watering_control_service,
    &wifi_ota_handler_service, 
    &wifi_server_handler_service,
    };

void setup()
{
    // write your initialization code here
    Serial.begin(512000);
    Serial.println();
    SPIFFS.begin();
    temperature_config_load(&temperature_config);
    watering_config_load(&watering_config);
    pins_init();
    temperature_sensor_init();
    wifi_connect(wifi_credentials);
    wifi_ota_init();
    wifi_server_init();

    systemu_scheduler_insert_services(&scheduler, services, sizeof(services) / sizeof(Service *));

    delay(1000);
}

void loop()
{
    // write your code here
    systemu_scheduler_run(&scheduler);
    // delay(500);
}
