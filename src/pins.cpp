#include <Arduino.h>
#include "pins.h"

void pins_init() {
    pinMode(pins.FAN_PIN, OUTPUT);
    pinMode(pins.HEATER_PIN, OUTPUT);
    pinMode(pins.PUMP_PIN, OUTPUT);
    digitalWrite(pins.FAN_PIN, LOW);
    digitalWrite(pins.HEATER_PIN, LOW);
    digitalWrite(pins.PUMP_PIN, LOW);
    pinMode(pins.DHT22_PIN, INPUT);
    pinMode(pins.MOISTURE_PIN, INPUT);
}