#ifndef GREENHOUSE_PINS_H
#define GREENHOUSE_PINS_H
#include <stdint.h>
#include <Arduino.h>

const struct {
    const uint8_t FAN_PIN = 4;
    const uint8_t PUMP_PIN = 12;
    const uint8_t HEATER_PIN = 5;
    const uint8_t DHT22_PIN = 13;
    const uint8_t MOISTURE_PIN = A0;
} pins;

extern void pins_init();

#endif /* GREENHOUSE_PINS_H */