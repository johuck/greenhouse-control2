#include "greenhouse.h"
#include "pins.h"
#include <stdint.h>
#include <systemu.h>
#include <ArduinoJson.h>
#include <FS.h>

DHT dht(pins.DHT22_PIN, DHT22);

void _temperature_heat() {
    Serial.println("Heating...");
    digitalWrite(pins.HEATER_PIN, HIGH);
    digitalWrite(pins.FAN_PIN, LOW);
    temperature_config.manual_state = TEMPERATURE_MANUAL_MODE_HEATING;
}

void _temperature_cool() {
    Serial.println("Cooling...");
    digitalWrite(pins.FAN_PIN, HIGH);
    digitalWrite(pins.HEATER_PIN, LOW);
    temperature_config.manual_state = TEMPERATURE_MANUAL_MODE_COOLING;
}

void _temperature_neutral() {
    Serial.println("Neutral...");
    digitalWrite(pins.HEATER_PIN, LOW);
    digitalWrite(pins.FAN_PIN, LOW);
    temperature_config.manual_state = TEMPERATURE_MANUAL_MODE_NEUTRAL;
}

void temperature_control_handler(uint32_t time, void *data /* TemperatureConfig */) {
    TemperatureConfig *cfg = (TemperatureConfig *) data;
    if (cfg->control_mode == CONTROL_MODE_AUTOMATIC) {
        temperature_control_automatic(cfg);
    } else if (cfg->control_mode == CONTROL_MODE_MANUAL) {
        temperature_control_manual(cfg->manual_state);
    } else {
        temperature_control_manual(TEMPERATURE_MANUAL_MODE_NEUTRAL);
    }
}

void temperature_control_automatic(TemperatureConfig *cfg)
{
    if (cfg->current_temperature < cfg->target - cfg->tolerance) {
        _temperature_heat();
    } else if (cfg->current_temperature > cfg->target + cfg->tolerance) {
        _temperature_cool();
    } else {
        _temperature_neutral();
    }
}

void temperature_control_manual(TemperatureManualState state) {
    if (state == TEMPERATURE_MANUAL_MODE_COOLING) {
        _temperature_cool();
    } else if (state == TEMPERATURE_MANUAL_MODE_HEATING) {
        _temperature_heat();
    } else {
        _temperature_neutral();
    }
}

void temperature_update_handler(uint32_t time, void *data /* TemperatureConfig */) {
    TemperatureConfig *profile = (TemperatureConfig *) data;
    float t = dht.readTemperature(profile->farenheit);
    float h = dht.readHumidity();
    if (isnan(t)) {
        Serial.println("Temperature is nan");
    } else {
        Serial.print("Got temperature: ");
        Serial.println(t);
        profile->current_temperature = t;
    }
    if (isnan(h)) {
        Serial.println("Humidity is nan");
    } else {
        Serial.print("Got Humidity: ");
        Serial.println(h);
        profile->current_humidity = h;
    }
}

void temperature_sensor_init() {
    dht.begin();
}

Service temperature_updater_service = {
        TEMPERATURE_UPDATER_SERVICE,    /* id */
        &temperature_update_handler,    /* run() */
        0,                              /* last_run */
        3000,                           /* interval */
        &temperature_config,            /* data (TemperatureConfig) */
        true                            /* enabled */
};

Service temperature_control_service = {
        TEMPERATURE_CONTROL_SERVICE,    /* id */
        &temperature_control_handler,   /* run() */
        0,                              /* last_run */
        3000,                           /* interval */
        &temperature_config,            /* data (TemperatureConfig) */
        true                            /* enabled */
};

String temperature_config_to_json(TemperatureConfig* config, bool settings_only) {
    StaticJsonDocument<256> doc;
    if (!settings_only) {
        doc["currentTemperature"] = config->current_temperature;
        doc["currentHumidity"] = config->current_humidity;
    }

    doc["target"] = config->target;
    doc["tolerance"] = config->tolerance;
    doc["farenheit"] = config->farenheit;
    doc["controlMode"] = config->control_mode;
    doc["manualState"] = config->manual_state;

    String json;
    serializeJson(doc, json);
    return json;
}

void temperature_config_from_json(TemperatureConfig* config, String json) {
    StaticJsonDocument<256> doc;
    deserializeJson(doc, json);
    
    if (!doc["currentTemperature"].isNull()) {
        config->current_temperature = doc["currentTemperature"];
    }
    if (!doc["currentHumidity"].isNull()) {
        config->current_humidity = doc["currentHumidity"];
    }
    if (!doc["target"].isNull()) {
        config->target = doc["target"];
    }
    if (!doc["tolerance"].isNull()) {
        config->tolerance = doc["tolerance"];
    }
    if (!doc["farenheit"].isNull()) {
        config->farenheit = doc["farenheit"];
    }
    if (!doc["controlMode"].isNull()) {
        config->control_mode = doc["controlMode"];
    }
    if (!doc["manualState"].isNull()) {
        config->manual_state = doc["manualState"];
    }
}


void temperature_config_save(TemperatureConfig* config) {
    String json = temperature_config_to_json(config, true);
    File save_file = SPIFFS.open(TEMPERATURE_CONFIG_SAVE_PATH, "w");
    save_file.write(json.c_str());
    save_file.close();
}

void temperature_config_load(TemperatureConfig* config) {
    if (SPIFFS.exists(TEMPERATURE_CONFIG_SAVE_PATH)) {
        File save_file = SPIFFS.open(TEMPERATURE_CONFIG_SAVE_PATH, "r");
        String json = save_file.readString();
        temperature_config_from_json(config, json);
        save_file.close();
    }
}