/**
 * @file wifi.cpp
 * @author Jonas Hucklenbroich (jonas@hucklenbroich.tk)
 * @brief Wifi functionality
 * @version 0.1
 * @date 2022-01-23
 * 
 * @copyright Copyright (c) 2022 Jonas Hucklenbroich
 * 
 */

#include "greenhouse.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoOTA.h>
#include <stdint.h>
#include <ArduinoJson.h>



void wifi_connect(WifiCredentials wifi_credentials)
{
    WiFi.begin((const char *)wifi_credentials.ssid, (const char *)wifi_credentials.password);
    Serial.print("Connecting to: ");
    Serial.println(wifi_credentials.ssid);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println();

    Serial.println("Connected, IP address: ");
    Serial.println(WiFi.localIP());
}


void wifi_ota_init()
{
    ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH)
            type = "sketch";
        else // U_SPIFFS
            type = "filesystem";

        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR)
            Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)
            Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR)
            Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR)
            Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR)
            Serial.println("End Failed");
    });
    ArduinoOTA.begin();
}

/**
 * @brief Handling OTA updates. Has to be called regularly (gets currently handled by systemu)
 * 
 * @param time (provided by systemu)
 * @param data (provided by systemu (probably NULL))
 */
void _wifi_ota_handle_func(uint32_t time, void *data)
{
    ArduinoOTA.handle();
}

Service wifi_ota_handler_service = {
    0,                     /* id */
    &_wifi_ota_handle_func, /* run() */
    0,                     /* last_run */
    500,                   /* interval */
    NULL,                  /* data */
    true                   /* enabled */
};

ESP8266WebServer server(80);

void apply_cors_header();

/**
 * @brief Handles get requests to '/temperature-config'
 * @response: {
 *   "target": number,              // The target temperature
 *   "tolerance": number,           // The tolerance
 *   "current-temperature": number, // The actual current temperature
 *   "current-humidity": number,    // The current humidity
 *   "farenheit": boolean,          // If true: Farenheit is used, Else if false: Celsius is used
 *   "control-mode": number,        // Whether it is controlled manually or automatic (or disabled) (see -> ControlMode)
 *   "manual-state": number          // The current manual mode (heating/cooling/neutral) ONLY applys if CONTROL-MODE is MANUAL! (see -> TemperatureManualState)
 * }
 */
void wifi_server_temperature_config_handler_get()
{
    StaticJsonDocument<1024> doc;
    doc["target"] = temperature_config.target;
    doc["tolerance"] = temperature_config.tolerance;
    doc["current-temperature"] = temperature_config.current_temperature;
    doc["current-humidity"] = temperature_config.current_humidity;
    doc["farenheit"] = temperature_config.farenheit;
    doc["control-mode"] = temperature_config.control_mode;
    doc["manual-state"] = temperature_config.manual_state;
    String json;
    serializeJson(doc, json);
    apply_cors_header();
    server.send(200, "application/json", json.c_str());
}

/**
 * @brief Handles put requests to '/temperature-config'
 * 
 * The following body has to be provided:
 * Optional fields are marked with '?' before their type
 * @body: {
 *   "target": number,
 *   "tolerance": number,
 *   "control-mode": ?number,
 *   "manual-state": ?number
 * }
 * 
 */
void wifi_server_temperature_config_handler_put() {
    if (!server.hasArg("plain")) {
        server.send(500, "text/plain", "ERROR: Body is empty");
        return;
    }
    String body = server.arg("plain");
    StaticJsonDocument<1024> doc;
    deserializeJson(doc, body);
    if (!doc["target"].isNull()) {
        temperature_config.target = doc["target"];
    }
    if (!doc["tolerance"].isNull()) {
        temperature_config.tolerance = doc["tolerance"];
    }
    if (!doc["control-mode"].isNull()) {
        ControlMode cm = doc["control-mode"];
        if (cm >= 0 && cm <= 2) {
            temperature_config.control_mode = cm;
        } else {
            server.send(500, "text/plain", "Error: control-mode is out of range. (0-2)");
            return;
        }
    }

    if (!doc["manual-state"].isNull()) {
        TemperatureManualState tms = doc["manual-state"];
        if (tms >= 0 && tms <= 2) {
            temperature_config.manual_state = tms;
        } else {
            server.send(500, "text/plain", "Error: manual-state is out of range. (0-2)");
            return;
        }
    }
    temperature_config_save(&temperature_config);
    apply_cors_header();
    server.send(200, "text/plain", "OK");
}

/**
 * @brief Handles get requests to '/watering-config'
 * 
 * @response: {
 *   "control-mode": number,
 *   "min-percentage": number,
 *   "max-percentage": number,
 *   "current-percentage": number,
 *   "manual-state": boolean,
 *   "sensor-calibration": {
 *     "min": number,
 *     "max": number
 *   }
 * }
 * 
 */
void wifi_server_watering_config_get() {
    StaticJsonDocument<1024> doc;
    doc["control-mode"] = watering_config.control_mode;
    doc["min-percentage"] = watering_config.min_percentage;
    doc["max-percentage"] = watering_config.max_percentage;
    doc["current-percentage"] = watering_config.current_percentage;
    doc["manual-state"] = watering_config.manual_state;
    doc["sensor-calibration"]["min"] = watering_config.sensor_calibration.min;
    doc["sensor-calibration"]["max"] = watering_config.sensor_calibration.max;

    String json;
    serializeJson(doc, json);
    apply_cors_header();
    server.send(200, "application/json", json.c_str());
}

/**
 * @brief Handles put requests to '/watering-config'
 * 
 * The following body has to be provided:
 * Optional fields are marked with '?' before their type
 * @body: {
 *   "control-mode": ?number,
 *   "min-percentage": ?number,
 *   "max-percentage": ?number,
 *   "manual-state": ?boolean,
 *   "sensor-calibration": {
 *     "min": ?number,  
 *     "max": ?number
 *   }
 * }
 */
void wifi_server_watering_config_put() {
    if (!server.hasArg("plain")) {
        server.send(500, "text/plain", "ERROR: Body is empty");
        return;
    }
    String body = server.arg("plain");
    StaticJsonDocument<1024> doc;
    deserializeJson(doc, body);

    if (!(doc["control-mode"].isNull())) {
        ControlMode cm = doc["control-mode"];
        if (cm >= 0 && cm <= 2) {
            watering_config.control_mode = cm;
        } else {
            server.send(500, "text/plain", "Error: control-mode is out of range. (0-2)");
            return;
        }
        Serial.println("control-mode is not null");
    }
    if (!(doc["min-percentage"].isNull())) {
        watering_config.min_percentage = doc["min-percentage"];
    }
    if (!(doc["max-percentage"].isNull())) {
        watering_config.max_percentage = doc["max-percentage"];
    }
    if (!(doc["manual-state"].isNull())) {
        watering_config.manual_state = doc["manual-state"];
    }
    if (!(doc["sensor-calibration"]["min"].isNull())) {
        watering_config.sensor_calibration.min = doc["sensor-calibration"]["min"];
    }
    if (!(doc["sensor-calibration"]["max"].isNull())) {
        watering_config.sensor_calibration.max = doc["sensor-calibration"]["max"];
    }
    watering_config_save(&watering_config);
    apply_cors_header();
    server.send(200, "text/plain", "OK");
}

/**
 * @brief Handles get requests to '/watering-config/calibrate/min'
 * 
 * Sets the current raw moisture value as the calibrated minimum
 * 
 * You should dry the sensor before doing this.
 * 
 */
void wifi_server_watering_calibrate_min_get() {
    watering_calibrate_min(&watering_config.sensor_calibration);
    watering_config_save(&watering_config);
    apply_cors_header();
    server.send(200, "text/plain", "OK");
}

/**
 * @brief Handles get requests to '/watering-config/calibrate/max'
 * 
 * Sets the current raw moisture value as the calibrated maximum
 * 
 * You should put the sensor into water before doing this.
 * 
 */
void wifi_server_watering_calibrate_max_get() {
    watering_calibrate_max(&watering_config.sensor_calibration);
    watering_config_save(&watering_config);
    apply_cors_header();
    server.send(200, "text/plain", "OK");
}


/**
 * @brief Handles clients connecting to the server. Has to be called regularly (gets handled by systemu)
 * itrobably NULL))
 */
void _wifi_server_client_handler_func(uint32_t time, void *data)
{
    server.handleClient();
}

void apply_cors_header() {
    server.sendHeader("Access-Control-Allow-Origin", F("*"));
    server.sendHeader("Access-Control-Max-Age", F("600"));
    server.sendHeader("Access-Control-Allow-Methods", F("PUT,GET,OPTIONS"));
    server.sendHeader("Access-Control-Allow-Headers", F("*"));
}

void send_cors_header() {
    apply_cors_header();
    server.send(204);
}

void wifi_server_init()
{

    server.on("/temperature-config", HTTP_GET, wifi_server_temperature_config_handler_get);
    server.on("/temperature-config", HTTP_PUT, wifi_server_temperature_config_handler_put);
    server.on("/temperature-config", HTTP_OPTIONS, send_cors_header);
    
    server.on("/watering-config", HTTP_GET, wifi_server_watering_config_get);
    server.on("/watering-config", HTTP_PUT, wifi_server_watering_config_put);
    server.on("/watering-config", HTTP_OPTIONS, send_cors_header);
    
    server.on("/watering-config/calibrate/min", HTTP_GET, wifi_server_watering_calibrate_min_get);
    server.on("/watering-config/calibrate/min", HTTP_OPTIONS, send_cors_header);

    server.on("/watering-config/calibrate/max", HTTP_GET, wifi_server_watering_calibrate_max_get);
    server.on("/watering-config/calibrate/max", HTTP_OPTIONS, send_cors_header);
    
    server.begin();
}

Service wifi_server_handler_service = {
    0,                                /* id */
    &_wifi_server_client_handler_func, /* run() */
    0,                                /* last_run */
    20,                              /* interval */
    NULL,                             /* data */
    true                              /* enabled */
};