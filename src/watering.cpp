#include <Arduino.h>
#include "greenhouse.h"
#include "pins.h"
#include <ArduinoJson.h>
#include <FS.h>

/**
 * This function reads the analog value from the sensor and inverts it
 * since the voltage rises when the sensor gets dry, which is unintuitive.
 * Thus we invert it which makes other stuff easier.
 */
int watering_read_moisture_raw() {
    // analog range on ESP8266 is from 0 to 1023
    return 1023 - analogRead(pins.MOISTURE_PIN);
}

void watering_moisture_update_handler(uint32_t time, void *data /* WateringConfig */) {
    WateringConfig *cfg = (WateringConfig*) data;
    int raw_value = watering_read_moisture_raw();
    Serial.print("raw moisture value: ");
    Serial.println(raw_value);
    watering_config.current_percentage = watering_calculate_percentage_calibration(watering_config.sensor_calibration, raw_value);
}

int watering_calculate_percentage_calibration(MoistureSensorCalibration& calibration, int raw_value) {
    float result = raw_value - calibration.min;
    if (result <= 0) {
        result = 0;
        Serial.println("Moisture lower 0");
    }
    
    result = (result / (calibration.max - calibration.min)) * 100;
    if (result > 100) {
        result = 100;
    }
    Serial.print("Calculated percentage of ");
    Serial.print(result);
    Serial.println("%");
    
    return result;
}

void watering_control_handler(uint32_t time, void *data /* WateringConfig */) {
    WateringConfig *cfg = (WateringConfig*) data;
    if (cfg->control_mode == CONTROL_MODE_AUTOMATIC) {
        watering_control_automatic(cfg);
    } else if (cfg->control_mode == CONTROL_MODE_MANUAL) {
        watering_control_manual(cfg->manual_state);
    } else {
        watering_control_manual(false);
    }
}

void watering_control_automatic(WateringConfig *cfg) {
    if (cfg->current_percentage < cfg->min_percentage) {
        digitalWrite(pins.PUMP_PIN, HIGH);
        watering_config.manual_state = true;
    } else if (cfg->current_percentage > cfg->max_percentage) {
        digitalWrite(pins.PUMP_PIN, LOW);
        watering_config.manual_state = false;
    }
}

void watering_control_manual(bool manual_state) {
    if (manual_state == true) {
        digitalWrite(pins.PUMP_PIN, HIGH);
    } else if (manual_state == false) {
        digitalWrite(pins.PUMP_PIN, LOW);
    }
}

void watering_calibrate_min(MoistureSensorCalibration *calibration) {
    int raw_value = watering_read_moisture_raw();
    calibration->min = raw_value;
}

void watering_calibrate_max(MoistureSensorCalibration *calibration) {
    int raw_value = watering_read_moisture_raw();
    calibration->max = raw_value;
}

Service watering_moisture_update_service = {
        WATERING_MOISTURE_UPDATE_SERVICE,   /* id */
        &watering_moisture_update_handler,  /* run() */
        0,                                  /* last_run */
        1000,                               /* interval */
        &watering_config,                   /* data (WateringConfig) */
        true                                /* enabled */
};

Service watering_control_service = {
        WATERING_CONTROL_SERVICE,   /* id */
        &watering_control_handler,  /* run() */
        0,                          /* last_run */
        1000,                       /* interval */
        &watering_config,           /* data (WateringConfig) */
        true                        /* enabled */
};

String watering_config_to_json(WateringConfig* config, bool settings_only) {
    StaticJsonDocument<256> doc;

    if (!settings_only) {
        doc["currentPercentage"] = config->current_percentage;
    }

    doc["minPercentage"] = config->min_percentage;
    doc["maxPercentage"] = config->max_percentage;
    doc["manualState"] = config->manual_state;
    doc["controlMode"] = config->control_mode;
    doc["sensorCalibration"]["min"] = config->sensor_calibration.min;
    doc["sensorCalibration"]["max"] = config->sensor_calibration.max;

    String json; 
    serializeJson(doc, json);
    return json;
}

void watering_config_from_json(WateringConfig* config, String json) {
    StaticJsonDocument<256> doc;
    deserializeJson(doc, json);

    if (!doc["currentPercentage"].isNull()) {
        config->current_percentage = doc["currentPercentage"];
    }
    if (!doc["minPercentage"].isNull()) {
        config->min_percentage = doc["minPercentage"];
    }
    if (!doc["maxPercentage"].isNull()) {
        config->max_percentage = doc["maxPercentage"];
    }
    if (!doc["manualState"].isNull()) {
        config->manual_state = doc["manualState"];
    }
    if (!doc["controlMode"].isNull()) {
        config->control_mode = doc["controlMode"];
    }
    if (!doc["sensorCalibration"]["min"].isNull()) {
        config->sensor_calibration.min = doc["sensorCalibration"]["min"];
    }
    if (!doc["sensorCalibration"]["max"].isNull()) {
        config->sensor_calibration.max = doc["sensorCalibration"]["max"];
    }

}

void watering_config_save(WateringConfig* config) {
    String json = watering_config_to_json(config, true);
    File save_file = SPIFFS.open(WATERING_CONFIG_SAVE_PATH, "w");
    save_file.write(json.c_str());
    save_file.close();
}

void watering_config_load(WateringConfig* config) {
    if (SPIFFS.exists(WATERING_CONFIG_SAVE_PATH)) {
        File save_file = SPIFFS.open(WATERING_CONFIG_SAVE_PATH, "r");
        String json = save_file.readString();
        watering_config_from_json(config, json);
        save_file.close();
    }
}