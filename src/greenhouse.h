#ifndef GREENHOUSE_MAIN_H
#define GREENHOUSE_MAIN_H

#include <Arduino.h>
#include <DHT.h>
#include <systemu.h>
#include <stdint.h>

extern Scheduler scheduler;

typedef enum {
    CONTROL_MODE_DISABLED = 0,
    CONTROL_MODE_AUTOMATIC = 1,
    CONTROL_MODE_MANUAL = 2,
} ControlMode;


enum service_ids
{
    TEMPERATURE_UPDATER_SERVICE = 0,
    TEMPERATURE_CONTROL_SERVICE = 10,
    WATERING_MOISTURE_UPDATE_SERVICE = 20,
    WATERING_CONTROL_SERVICE = 30,
    WIFI_OTA_SERVICE = 40
};

/**
 *  Temperature
 * #################################
 */

typedef enum {
    TEMPERATURE_MANUAL_MODE_NEUTRAL = 0,
    TEMPERATURE_MANUAL_MODE_COOLING = 1,
    TEMPERATURE_MANUAL_MODE_HEATING = 2,
} TemperatureManualState;

typedef struct
{
    ControlMode control_mode;
    bool farenheit;
    float current_temperature;
    float current_humidity;
    float target;
    float tolerance;
    TemperatureManualState manual_state;
} TemperatureConfig;


extern Service temperature_updater_service;
extern Service temperature_control_service;

extern TemperatureConfig temperature_config;

extern String temperature_config_to_json(TemperatureConfig* config, bool settings_only);
extern void temperature_config_from_json(TemperatureConfig* config, String json);

#define TEMPERATURE_CONFIG_SAVE_PATH "temperature-config.json"
extern void temperature_config_save(TemperatureConfig* config);
extern void temperature_config_load(TemperatureConfig* config);

/**
 * @brief Initializes the temperature sensor
 * 
 */
extern void temperature_sensor_init();

/**
 * @brief Monitors the temperature and reacts to it based on the TemperatureConfig
 * 
 * @param time (provided by systemu)
 * @param data (provided by systemu (*TemperatuerConfig))
 * 
 */
extern void temperature_control_handler(uint32_t time, void *data /* TemperatureConfig */);

/**
 * @brief Controlls the temperature in AUTOMATIC MODE based on the TemperatureConfig provided
 * 
 * @param profile The TemperatureConfig used to control the temperature
 */
extern void temperature_control_automatic(TemperatureConfig *profile);

/**
 * @brief Controlls the temperature in MANUAL MODE based on the TemperatureManualState provided
 * 
 * @param mode The TemperatureManualState which ist directly applyed
 */
extern void temperature_control_manual(TemperatureManualState mode);

/**
 * @brief Requests the temperature from the sensor and updates the TemperatuerProfile (handled by systemu)
 * @param time (provided by systemu)
 * @param data (provided by systemu)
 */
extern void temperature_update_handler(uint32_t time, void *data /* TemperatureConfig */);


/**
 *  Watering
 * #################################
 */


typedef struct {
    int min;
    int max;
} MoistureSensorCalibration;

typedef struct
{
    ControlMode control_mode;
    int min_percentage;
    int max_percentage;
    int current_percentage;
    bool manual_state;
    MoistureSensorCalibration sensor_calibration;
} WateringConfig;

extern Service watering_moisture_update_service;
extern Service watering_control_service;

extern WateringConfig watering_config;

extern String watering_config_to_json(WateringConfig* config, bool settings_only);
extern void watering_config_from_json(WateringConfig* config, String json);

#define WATERING_CONFIG_SAVE_PATH "/watering-config.json"

extern void watering_config_save(WateringConfig* config);
extern void watering_config_load(WateringConfig* config);

/**
 * @brief Updates the current_percentage moisture value in the WateringConfig
 * 
 */
extern void watering_moisture_update_handler(uint32_t time, void *data /* WateringConfig */);

/**
 * @brief Reads the raw sensor value
 * 
 * @return int the analog sensor value (correctly inverted) from 0 to 1023
 */
extern int watering_read_moisture_raw();

/**
 * @brief Calculates the moisture percentage from the raw value and the calibration data
 * 
 * @param calibration 
 * @param raw_value 
 * @return int 
 */
extern int watering_calculate_percentage_calibration(MoistureSensorCalibration& calibration, int raw_value);

/**
 * @brief Monitors the moisture level and reacts to it based on the WateringConfig
 * 
 */
extern void watering_control_handler(uint32_t time, void *data /* WateringConfig */);

/**
 * @brief Switches the pump based on the automatic configuration in WateringConfig
 * 
 * @param cfg 
 */
extern void watering_control_automatic(WateringConfig *cfg);

/**
 * @brief Switches the pump based on the manual configuration in WateringConfig
 * 
 * @param cfg 
 */
extern void watering_control_manual(bool manual_state);

/**
 * @brief Sets the currently read sensor data as the new calibrated minimum
 * 
 * Dry the sensor and then call this function.
 * 
 * @param calibration 
 */
extern void watering_calibrate_min(MoistureSensorCalibration *calibration);

/**
 * @brief Sets the currently read sensor data as the new calibrated maximum
 * 
 * Put the sensor in water and then call this function.
 * 
 * @param calibration 
 */
extern void watering_calibrate_max(MoistureSensorCalibration *calibration);

/**
 *  WiFi and API                   
 * #################################
 */

typedef struct
{
    char ssid[1000];
    char password[1000];
} WifiCredentials;

extern Service wifi_ota_handler_service;

/**
 * @brief Connects to the WiFi Network
 * 
 * @param wifi_credentials The credentials to use to connect to the network
 */
extern void wifi_connect(WifiCredentials wifi_credentials);

/**
 * @brief Initializes OTA updates
 * 
 */
extern void wifi_ota_init();

/**
 * @brief Initializes the API server
 * 
 */
extern void wifi_server_init();

/**
 * @brief Service that handles clients connecting to the API server
 * 
 */
extern Service wifi_server_handler_service;


#endif /* GREENHOUSE_MAIN_H */
