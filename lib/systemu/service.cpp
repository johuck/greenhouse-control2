#include "systemu.h"
#include <malloc.h>
#include <HardwareSerial.h>
#include <stdint.h>

Service *systemu_service_create(int id, void (*run)(uint32_t, void*), uint32_t interval, void* data) {
    Service* service = (Service*)malloc(sizeof (Service));
    service->id = id;
    service->interval = interval;
    service->last_run = 0;
    service->run = run;
    service->data = data;
    return service;
}

void systemu_service_destroy(Service* service) {
    free(service);
}


void systemu_service_disable(Service* service) {
    service->enabled = false;
}

void systemu_service_enable(Service* service) {
    service->enabled = true;
}