#ifndef GREENHOUSE_SYSTEMU_H
#define GREENHOUSE_SYSTEMU_H

#include <stdint.h>



typedef struct {
    uint8_t id;
    void (*run)(uint32_t, void*);
    uint32_t last_run;
    uint32_t interval;
    void* data;
    bool enabled;
} Service;

typedef struct {
    Service** services;
    uint8_t service_count;
} Scheduler;

// Service

extern Service* systemu_service_create(void (*run)(Scheduler, uint32_t, void*), uint32_t interval, void* data);
extern void systemu_service_destroy(Service* service);

extern void systemu_service_disable(Service* service);
extern void systemu_service_enable(Service* service);

// Scheduler

extern Scheduler* systemu_scheduler_create();
extern Scheduler* systemu_scheduler_create_with_services(Service** services);
extern void systemu_scheduler_destroy(Scheduler* scheduler);

extern void systemu_scheduler_insert_services(Scheduler* scheduler, Service** services, uint8_t count);
extern void systemu_scheduler_insert_service(Scheduler* scheduler, Service* service);

extern Service* systemu_scheduler_get_service(int id);

extern void systemu_scheduler_run(Scheduler* scheduler);

#endif /* GREENHOUSE_SYSTEMU_H */