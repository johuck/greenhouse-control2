#include "systemu.h"
#include <malloc.h>
#include <Arduino.h>
#include <stdint.h>

Scheduler* systemu_scheduler_create() {
    Scheduler* scheduler = (Scheduler*)malloc(sizeof (Scheduler));
    scheduler->service_count = 0;
    return scheduler;
}

Scheduler* systemu_scheduler_create_with_services(Service** services) {
    Scheduler* scheduler = (Scheduler*)malloc(sizeof (Scheduler));
    scheduler->service_count = 0;
    scheduler->services = services;
    return scheduler;
}

void systemu_scheduler_destroy(Scheduler* scheduler) {
    free(scheduler);
}

void systemu_scheduler_insert_services(Scheduler* scheduler, Service** services, uint8_t count) {
    scheduler->services = services;
    scheduler->service_count = count;
}

void systemu_scheduler_insert_service(Scheduler* scheduler, Service* service) {
    scheduler->service_count += 1;
    
    scheduler->services = (Service**)realloc(scheduler->services, (sizeof *service) * scheduler->service_count);
    
    *(scheduler->services + (sizeof(Service*) * (scheduler->service_count-1))) = service;
}

void systemu_scheduler_run(Scheduler* scheduler) {
    uint32 current_time = millis();

    for (int i = 0; i < scheduler->service_count; i++) {
        Service* service = scheduler->services[i];
        if (service == NULL) {
            Serial.println("BUG: Service NULL");
            continue;
        }

        if (!service->enabled) {
            continue;
        }

        if (service->last_run + service->interval <= current_time || current_time < service->last_run) {
            // if (service->interval > 100) {
            //     Serial.println("Running something"); 
            
            //     Serial.print("Service last_run: ");
            //     Serial.println(service->last_run);
            //     Serial.print("Service interval: ");
            //     Serial.println(service->interval);
            // }

            service->run(current_time, service->data);
            service->last_run = current_time;
        }
    }
}

Service* systemu_scheduler_get_service(Scheduler* scheduler, int id) {
    Service* service = *scheduler->services;
    for (void* end = (service + (sizeof service)*scheduler->service_count); end != service; service++) {
        if (service == NULL) {
            Serial.println("BUG: Service == NULL");
            continue;
        }
        if (service->id == id) {
            return service;
        }
    }
    return NULL;
}